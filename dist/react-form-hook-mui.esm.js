import React, { createRef, useState, cloneElement, createElement } from 'react';
import { Drawer, createStyles, colors } from '@material-ui/core';
import { makeStyles, createStyles as createStyles$1 } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { Clear } from '@material-ui/icons';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import Backdrop from '@material-ui/core/Backdrop';
import DialogTitle from '@material-ui/core/DialogTitle';
import Close from '@material-ui/icons/Close';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

var useStyles = /*#__PURE__*/makeStyles(function (theme) {
  var _paper;

  return createStyles({
    paper: (_paper = {
      flexDirection: 'column',
      width: '100%'
    }, _paper[theme.breakpoints.up('sm')] = {
      width: '30vw'
    }, _paper),
    title: {
      flexGrow: 1
    },
    drawerContent: {
      flexGrow: 1,
      overflowY: 'auto',
      '& form': {
        padding: theme.spacing(3)
      }
    }
  });
});
var DialogContainerDrawerWrap = function DialogContainerDrawerWrap(_ref) {
  var children = _ref.children,
      open = _ref.open,
      disableClickOutside = _ref.disableClickOutside,
      drawerProps = _ref.drawerProps,
      setOpen = _ref.setOpen,
      title = _ref.title;
  var classes = useStyles();
  var ref = createRef();
  return React.createElement(Drawer, Object.assign({}, drawerProps, {
    anchor: "right",
    open: open,
    PaperProps: {
      className: classes.paper
    },
    onClose: function onClose() {
      return !disableClickOutside && setOpen(false);
    }
  }), React.createElement(AppBar, {
    color: 'primary',
    position: 'relative',
    variant: 'outlined'
  }, React.createElement(Toolbar, null, React.createElement(Typography, {
    variant: "h6",
    className: classes.title
  }, title), React.createElement(IconButton, {
    color: 'inherit',
    onClick: function onClick() {
      return setOpen(false);
    }
  }, React.createElement(Clear, null)))), React.createElement("div", {
    className: classes.drawerContent,
    ref: ref
  }, children));
};

var useStyle = /*#__PURE__*/makeStyles(function (theme) {
  return createStyles$1({
    dialogTitle: {
      margin: 0,
      padding: theme.spacing(2)
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1)
    }
  });
});
var DialogContainerDialogWrap = function DialogContainerDialogWrap(_ref) {
  var title = _ref.title,
      dialogProps = _ref.dialogProps,
      disableClickOutside = _ref.disableClickOutside,
      setOpen = _ref.setOpen,
      open = _ref.open,
      children = _ref.children;
  var classes = useStyle();
  return React.createElement(Dialog, Object.assign({
    open: open,
    onClose: function onClose() {
      return !disableClickOutside && setOpen(false);
    },
    closeAfterTransition: true,
    BackdropComponent: Backdrop,
    BackdropProps: {
      timeout: 500
    }
  }, dialogProps), React.createElement(DialogTitle, {
    disableTypography: true,
    className: classes.dialogTitle
  }, React.createElement(React.Fragment, null, title && React.createElement(Typography, {
    color: 'textSecondary',
    variant: 'h6'
  }, title), React.createElement(IconButton, {
    "aria-label": "close",
    className: classes.closeButton,
    onClick: function onClick() {
      return setOpen(false);
    }
  }, React.createElement(Close, null)))), React.createElement(DialogContent, null, children));
};

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function DialogContainer(_ref) {
  var TriggerEl = _ref.TriggerEl,
      ContentEl = _ref.ContentEl,
      renderAsSidebar = _ref.renderAsSidebar,
      afterClose = _ref.afterClose,
      rest = _objectWithoutPropertiesLoose(_ref, ["TriggerEl", "ContentEl", "renderAsSidebar", "afterClose"]);

  var _useState = useState(false),
      open = _useState[0],
      setOpen = _useState[1];

  return React.createElement(React.Fragment, null, TriggerEl && cloneElement(TriggerEl, {
    onClick: function onClick() {
      return setOpen(true);
    }
  }), createElement(renderAsSidebar ? DialogContainerDrawerWrap : DialogContainerDialogWrap, _extends({
    setOpen: setOpen,
    open: open
  }, rest), ContentEl && cloneElement(ContentEl, {
    closeDialog: function closeDialog() {
      setOpen(false);

      if (afterClose) {
        afterClose();
      }
    }
  })));
}

var SpinnerButton = function SpinnerButton(_ref) {
  var loading = _ref.loading,
      label = _ref.label,
      success = _ref.success,
      rest = _objectWithoutPropertiesLoose(_ref, ["loading", "label", "success"]);

  var additionalProps = {};

  if (success) {
    additionalProps.style = {
      backgroundColor: colors.green['400']
    };
  }

  return React.createElement(Button, Object.assign({}, rest, {
    color: rest.color || 'primary',
    type: rest.type || 'submit',
    disabled: !!loading,
    startIcon: success ? React.createElement(CheckCircleOutlineIcon, null) : undefined
  }, additionalProps), loading && React.createElement(CircularProgress, {
    size: 20
  }), "\xA0\xA0", label || 'Speichern');
};

export { DialogContainer, DialogContainerDialogWrap, DialogContainerDrawerWrap, SpinnerButton };
//# sourceMappingURL=react-form-hook-mui.esm.js.map
