'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var core = require('@material-ui/core');
var styles = require('@material-ui/core/styles');
var Toolbar = _interopDefault(require('@material-ui/core/Toolbar'));
var IconButton = _interopDefault(require('@material-ui/core/IconButton'));
var icons = require('@material-ui/icons');
var Typography = _interopDefault(require('@material-ui/core/Typography'));
var AppBar = _interopDefault(require('@material-ui/core/AppBar'));
var Dialog = _interopDefault(require('@material-ui/core/Dialog'));
var Backdrop = _interopDefault(require('@material-ui/core/Backdrop'));
var DialogTitle = _interopDefault(require('@material-ui/core/DialogTitle'));
var Close = _interopDefault(require('@material-ui/icons/Close'));
var DialogContent = _interopDefault(require('@material-ui/core/DialogContent'));
var CircularProgress = _interopDefault(require('@material-ui/core/CircularProgress'));
var Button = _interopDefault(require('@material-ui/core/Button'));
var CheckCircleOutlineIcon = _interopDefault(require('@material-ui/icons/CheckCircleOutline'));

var useStyles = /*#__PURE__*/styles.makeStyles(function (theme) {
  var _paper;

  return core.createStyles({
    paper: (_paper = {
      flexDirection: 'column',
      width: '100%'
    }, _paper[theme.breakpoints.up('sm')] = {
      width: '30vw'
    }, _paper),
    title: {
      flexGrow: 1
    },
    drawerContent: {
      flexGrow: 1,
      overflowY: 'auto',
      '& form': {
        padding: theme.spacing(3)
      }
    }
  });
});
var DialogContainerDrawerWrap = function DialogContainerDrawerWrap(_ref) {
  var children = _ref.children,
      open = _ref.open,
      disableClickOutside = _ref.disableClickOutside,
      drawerProps = _ref.drawerProps,
      setOpen = _ref.setOpen,
      title = _ref.title;
  var classes = useStyles();
  var ref = React.createRef();
  return React__default.createElement(core.Drawer, Object.assign({}, drawerProps, {
    anchor: "right",
    open: open,
    PaperProps: {
      className: classes.paper
    },
    onClose: function onClose() {
      return !disableClickOutside && setOpen(false);
    }
  }), React__default.createElement(AppBar, {
    color: 'primary',
    position: 'relative',
    variant: 'outlined'
  }, React__default.createElement(Toolbar, null, React__default.createElement(Typography, {
    variant: "h6",
    className: classes.title
  }, title), React__default.createElement(IconButton, {
    color: 'inherit',
    onClick: function onClick() {
      return setOpen(false);
    }
  }, React__default.createElement(icons.Clear, null)))), React__default.createElement("div", {
    className: classes.drawerContent,
    ref: ref
  }, children));
};

var useStyle = /*#__PURE__*/styles.makeStyles(function (theme) {
  return styles.createStyles({
    dialogTitle: {
      margin: 0,
      padding: theme.spacing(2)
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1)
    }
  });
});
var DialogContainerDialogWrap = function DialogContainerDialogWrap(_ref) {
  var title = _ref.title,
      dialogProps = _ref.dialogProps,
      disableClickOutside = _ref.disableClickOutside,
      setOpen = _ref.setOpen,
      open = _ref.open,
      children = _ref.children;
  var classes = useStyle();
  return React__default.createElement(Dialog, Object.assign({
    open: open,
    onClose: function onClose() {
      return !disableClickOutside && setOpen(false);
    },
    closeAfterTransition: true,
    BackdropComponent: Backdrop,
    BackdropProps: {
      timeout: 500
    }
  }, dialogProps), React__default.createElement(DialogTitle, {
    disableTypography: true,
    className: classes.dialogTitle
  }, React__default.createElement(React__default.Fragment, null, title && React__default.createElement(Typography, {
    color: 'textSecondary',
    variant: 'h6'
  }, title), React__default.createElement(IconButton, {
    "aria-label": "close",
    className: classes.closeButton,
    onClick: function onClick() {
      return setOpen(false);
    }
  }, React__default.createElement(Close, null)))), React__default.createElement(DialogContent, null, children));
};

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function DialogContainer(_ref) {
  var TriggerEl = _ref.TriggerEl,
      ContentEl = _ref.ContentEl,
      renderAsSidebar = _ref.renderAsSidebar,
      afterClose = _ref.afterClose,
      rest = _objectWithoutPropertiesLoose(_ref, ["TriggerEl", "ContentEl", "renderAsSidebar", "afterClose"]);

  var _useState = React.useState(false),
      open = _useState[0],
      setOpen = _useState[1];

  return React__default.createElement(React__default.Fragment, null, TriggerEl && React.cloneElement(TriggerEl, {
    onClick: function onClick() {
      return setOpen(true);
    }
  }), React.createElement(renderAsSidebar ? DialogContainerDrawerWrap : DialogContainerDialogWrap, _extends({
    setOpen: setOpen,
    open: open
  }, rest), ContentEl && React.cloneElement(ContentEl, {
    closeDialog: function closeDialog() {
      setOpen(false);

      if (afterClose) {
        afterClose();
      }
    }
  })));
}

var SpinnerButton = function SpinnerButton(_ref) {
  var loading = _ref.loading,
      label = _ref.label,
      success = _ref.success,
      rest = _objectWithoutPropertiesLoose(_ref, ["loading", "label", "success"]);

  var additionalProps = {};

  if (success) {
    additionalProps.style = {
      backgroundColor: core.colors.green['400']
    };
  }

  return React__default.createElement(Button, Object.assign({}, rest, {
    color: rest.color || 'primary',
    type: rest.type || 'submit',
    disabled: !!loading,
    startIcon: success ? React__default.createElement(CheckCircleOutlineIcon, null) : undefined
  }, additionalProps), loading && React__default.createElement(CircularProgress, {
    size: 20
  }), "\xA0\xA0", label || 'Speichern');
};

exports.DialogContainer = DialogContainer;
exports.DialogContainerDialogWrap = DialogContainerDialogWrap;
exports.DialogContainerDrawerWrap = DialogContainerDrawerWrap;
exports.SpinnerButton = SpinnerButton;
//# sourceMappingURL=react-form-hook-mui.cjs.development.js.map
