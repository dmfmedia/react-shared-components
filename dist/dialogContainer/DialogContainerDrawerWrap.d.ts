import { FC } from 'react';
import { DrawerProps } from '@material-ui/core';
export declare const DialogContainerDrawerWrap: FC<{
    title?: string;
    drawerProps?: Partial<DrawerProps>;
    disableClickOutside?: boolean;
    setOpen: Function;
    open: boolean;
}>;
export default DialogContainerDrawerWrap;
