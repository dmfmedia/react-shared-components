import { FC } from 'react';
import { DialogProps } from '@material-ui/core/Dialog';
export declare const DialogContainerDialogWrap: FC<{
    title?: string;
    dialogProps?: Partial<DialogProps>;
    disableClickOutside?: boolean;
    setOpen: Function;
    open: boolean;
}>;
export default DialogContainerDialogWrap;
