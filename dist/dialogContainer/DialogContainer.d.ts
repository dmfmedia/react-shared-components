import { ReactElement } from 'react';
import { DialogProps } from '@material-ui/core/Dialog';
import { DrawerProps } from '@material-ui/core';
export declare type DialogContainerProps = {
    TriggerEl: ReactElement;
    ContentEl: ReactElement;
    title?: string;
    dialogProps?: Partial<DialogProps>;
    drawerProps?: Partial<DrawerProps>;
    disableClickOutside?: boolean;
    renderAsSidebar?: boolean;
    afterClose?: () => void;
};
export declare function DialogContainer({ TriggerEl, ContentEl, renderAsSidebar, afterClose, ...rest }: DialogContainerProps): JSX.Element;
