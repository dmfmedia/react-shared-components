import { FC } from 'react';
import { ButtonProps } from '@material-ui/core/Button';
export declare type SpinnerButtonProps = ButtonProps & {
    loading?: boolean;
    label?: string;
    success?: boolean;
};
export declare const SpinnerButton: FC<ButtonProps & {
    loading?: boolean;
    label?: string;
    success?: boolean;
}>;
