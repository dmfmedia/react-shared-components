import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { SpinnerButton } from '../src'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <SpinnerButton />,
    div
  )
  ReactDOM.unmountComponentAtNode(div)
})
