import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { DialogContainer } from '../src'
import Button from '@material-ui/core/Button'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <DialogContainer ContentEl={<h3>Content</h3>} TriggerEl={<Button>Trigger</Button>} title={'Title'} />,
    div
  )
  ReactDOM.unmountComponentAtNode(div)
})
