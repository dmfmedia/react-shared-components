import 'react-app-polyfill/ie11'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {DialogContainer} from 'react-shared-components'
import Button from '@material-ui/core/Button'

type FormProps = {
  hallo: string
}
const App = () => {
  let defaultValues: FormProps = { hallo: '' }
  return (
    <div>
      <DialogContainer ContentEl={<h3>Content</h3>} TriggerEl={<Button>Trigger</Button>} title={'Title'} />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
