import React, {
  cloneElement,
  createElement,
  ReactElement,
  useState
} from 'react'
import { DialogProps } from '@material-ui/core/Dialog'
import DialogContainerDialogWrap from './DialogContainerDialogWrap'
import DialogContainerDrawerWrap from './DialogContainerDrawerWrap'
import { DrawerProps } from '@material-ui/core'

export type DialogContainerProps = {
  TriggerEl: ReactElement
  ContentEl: ReactElement
  title?: string
  dialogProps?: Partial<DialogProps>
  drawerProps?: Partial<DrawerProps>
  disableClickOutside?: boolean
  renderAsSidebar?: boolean
  afterClose?: () => void
}

export function DialogContainer ({ TriggerEl, ContentEl, renderAsSidebar, afterClose, ...rest }: DialogContainerProps): JSX.Element {
  const [open, setOpen] = useState<boolean>(false)
  return (
    <>
      {TriggerEl &&
        cloneElement(TriggerEl, {
          onClick: () => setOpen(true)
        })}
      {createElement(
        renderAsSidebar ? DialogContainerDrawerWrap : DialogContainerDialogWrap,
        {
          setOpen,
          open,
          ...rest
        },
        ContentEl &&
          cloneElement(ContentEl, {
            closeDialog: () => {
              setOpen(false)
              if(afterClose) {
                afterClose()
              }
            }
          })
      )}
    </>
  )
}
