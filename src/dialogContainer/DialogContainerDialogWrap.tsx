import React, { FC } from 'react'
import Dialog, { DialogProps } from '@material-ui/core/Dialog'
import Backdrop from '@material-ui/core/Backdrop'
import DialogTitle from '@material-ui/core/DialogTitle'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Close from '@material-ui/icons/Close'
import DialogContent from '@material-ui/core/DialogContent'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    dialogTitle: {
      margin: 0,
      padding: theme.spacing(2)
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1)
    }
  })
)

export const DialogContainerDialogWrap: FC<{
  title?: string
  dialogProps?: Partial<DialogProps>
  disableClickOutside?: boolean
  setOpen: Function
  open: boolean
}> = ({ title, dialogProps, disableClickOutside, setOpen, open, children }) => {
  const classes = useStyle()
  return (
    <Dialog
      open={open}
      onClose={() => !disableClickOutside && setOpen(false)}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
      {...dialogProps}
    >
      <DialogTitle disableTypography className={classes.dialogTitle}>
        <>
          {title && (
            <Typography color={'textSecondary'} variant={'h6'}>
              {title}
            </Typography>
          )}
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={() => setOpen(false)}
          >
            <Close />
          </IconButton>
        </>
      </DialogTitle>

      <DialogContent>{children}</DialogContent>
    </Dialog>
  )
}

export default DialogContainerDialogWrap
