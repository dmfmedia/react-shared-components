import React, { createRef, FC, RefObject } from 'react'
import { createStyles, Drawer, DrawerProps, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import { Clear } from '@material-ui/icons'
import Typography from '@material-ui/core/Typography'
import AppBar from '@material-ui/core/AppBar'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      flexDirection: 'column',
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '30vw'
      }
    },
    title: {
      flexGrow: 1
    },
    drawerContent: {
      flexGrow: 1,
      overflowY: 'auto',
      '& form': {
        padding: theme.spacing(3)
      }
    }
  })
)

export const DialogContainerDrawerWrap: FC<{
  title?: string
  drawerProps?: Partial<DrawerProps>
  disableClickOutside?: boolean
  setOpen: Function
  open: boolean
}> = ({ children, open, disableClickOutside, drawerProps, setOpen, title }) => {
  const classes = useStyles()
  const ref: RefObject<HTMLDivElement> = createRef()
  return (
    <Drawer
      {...drawerProps}
      anchor="right"
      open={open}
      PaperProps={{
        className: classes.paper
      }}
      onClose={() => !disableClickOutside && setOpen(false)}
    >
      <AppBar color={'primary'} position={'relative'} variant={'outlined'}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            {title}
          </Typography>
          <IconButton color={'inherit'} onClick={() => setOpen(false)}>
            <Clear />
          </IconButton>
        </Toolbar>
      </AppBar>
      <div className={classes.drawerContent} ref={ref}>
        {children}
      </div>
    </Drawer>
  )
}

export default DialogContainerDrawerWrap
