import React, { FC } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button, { ButtonProps } from '@material-ui/core/Button'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline'
import { colors } from '@material-ui/core'

export type SpinnerButtonProps =
  ButtonProps & {
  loading?: boolean
  label?: string
  success?: boolean
}

export const SpinnerButton: FC<
  ButtonProps & {
    loading?: boolean
    label?: string
    success?: boolean
  }
> = ({ loading, label, success, ...rest }) => {
  const additionalProps: ButtonProps = {}
  if (success) {
    additionalProps.style = {
      backgroundColor: colors.green['400']
    }
  }
  return (
    <Button
      {...rest}
      color={rest.color || 'primary'}
      type={rest.type || 'submit'}
      disabled={!!loading}
      startIcon={success ? <CheckCircleOutlineIcon /> : undefined}
      {...additionalProps}
    >
      {loading && <CircularProgress size={20} />}
      &nbsp;&nbsp;{label || 'Speichern'}
    </Button>
  )
}
