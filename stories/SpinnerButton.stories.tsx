import * as React from 'react'
import { SpinnerButton } from '../src'
import { useState } from 'react'

export default {
  title: 'SpinnerButton'
}

export const Basic = () => {
  const [loading, setLoading] = useState<boolean>(false)
  return (
    <SpinnerButton loading={loading} label={'Click to spin'} onClick={() => setLoading(!loading)}/>
  )
}


