import * as React from 'react'
import Button from '@material-ui/core/Button'
import { DialogContainer } from '../src'

export default {
  title: 'DialogContainer'
}

export const Basic = () => {
  return (
    <DialogContainer ContentEl={<h3>Content</h3>} TriggerEl={<Button>Trigger</Button>} title={'Title'} />
  )
}


